#!/bin/bash
#
# This runs a candidate image through a series of tests to ensure it is working as
# expected

if [ $# -ne 1 ]; then
  echo "Usage: ${0} <canidate-image>" >&2
  exit 2
fi

IMAGE_NAME=${1}

test_dir="$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)/test"

# Since we built the candidate image locally, we don't want S2I to attempt to pull
# it from Docker hub
s2i_args="--pull-policy=never --loglevel=2"

# Port the image exposes service to be tested
test_port=8080

check_result() {
  local app_name="$1"
  local result="$2"
  if [[ "$result" != "0" ]]; then
    echo "S2I image '${IMAGE_NAME}' (${app_name}) test FAILED (exit code: ${result})"
    if [ -f ${out_file} ]; then
      echo "Last output:"
      cat ${out_file}
    fi
    cleanup ${app_name}
    exit $result
  fi
}

cleanup() {
  local app_name="$1"
  if [ -f $cid_file ]; then
    if container_exists; then
      docker stop $(cat $cid_file) > /dev/null
      docker rm $(cat $cid_file) > /dev/null
    fi
  fi
  if image_exists ${IMAGE_NAME}-${app_name}; then
    docker rmi ${IMAGE_NAME}-${app_name} > /dev/null
  fi
  rm -rf ${tmp_dir}
}

container_exists() {
  image_exists $(cat $cid_file)
}

container_ip() {
  if [[ "$OSTYPE" =~ 'darwin' ]]; then
#    echo 'localhost'
    docker inspect --format="{{(index .NetworkSettings.Ports \"$test_port/tcp\" 0).HostIP}}" "$(cat "${cid_file}")"
  else
    docker inspect --format="{{ .NetworkSettings.IPAddress }}" $(cat $cid_file)
  fi
}

container_port() {
  if [[ "$OSTYPE" =~ 'darwin' ]]; then
    docker inspect --format="{{(index .NetworkSettings.Ports \"$test_port/tcp\" 0).HostPort}}" "$(cat "${cid_file}")"
  else
    echo $test_port
  fi
}

image_exists() {
  docker inspect $1 &>/dev/null
}

prepare() {
  local app_name="$1"

  if [ -f src/Dockerfile ] ; then
    run_docker_build
  else
    run_s2i_build ${app_name}
  fi
}

run_docker_build() {
  # We don't want to modify the Dockerfile in the source repo, and the version of docker
  # shipped with fedora doesn't support the '-f -' option, so we will copy the whole src
  # tree into our tmp directory and modify the Dockerfile there
  rm -rf ${tmp_dir}/src
  cp -a src ${tmp_dir}/src
  sed -ie "s/^FROM .*/FROM ${IMAGE_NAME}/" ${tmp_dir}/src/Dockerfile
  docker build -t ${IMAGE_NAME}-${app_name} ${tmp_dir}/src
}

run_s2i_build() {
  local app_name="$1"
  s2i build --incremental=true ${s2i_args} file://${test_dir}/${app_name}/src ${IMAGE_NAME} ${IMAGE_NAME}-${app_name}
}

run_test_application() {
  local app_name="$1"
  docker run --cidfile=${cid_file} -p ${test_port} --user=2358332:0 ${IMAGE_NAME}-${app_name} > /dev/null
}

test_entrypoint() {
  local expected_entrypoint="[/usr/bin/dumbinit --]"
  local entryponit=$(docker inspect --format="{{ .Config.EntryPoint }}" "$(cat "${cid_file}")")

  if [ ${entryponit} != ${expected_entrypoint} ]; then
    echo "Entrypoint is incorrect"
    echo "Expected entrypoint: ${expected_entrypoint}"
    echo "Actual entrypoint: ${entrypoint}"
    return 1
  fi
  return 0
}

test_http() {
  if [ ! -f expected-httpout ]; then
    return 0
  fi
  echo -n "Testing HTTP connection (http://$(container_ip):$(container_port))  "
  local max_attempts=10
  local sleep_time=1
  local attempt=1
  local result=1
  while [ $attempt -le $max_attempts ]; do
    echo -n "Sending GET request to http://$(container_ip):$(container_port)/  "
    echo "curl -s -w %{http_code} -o ${tmp_dir}/web_out http://$(container_ip):$(container_port)/"
    sleep 60
    response_code=$(curl -s -w %{http_code} -o ${tmp_dir}/web_out http://$(container_ip):$(container_port)/)
    status=$?
    if [ $status -eq 0 ]; then
      if [ $response_code -eq 200 ]; then
        result=0
      fi
      break
    fi
    attempt=$(( $attempt + 1 ))
    sleep $sleep_time
  done
  if ! diff -q ${tmp_dir}/web_out expected-httpout > /dev/null ; then
    echo "Expected output does match actual output"
    echo "Expected Output:"
    cat expected-httpout
    echo
    echo "Actual Output:"
    cat ${tmp_dir}/web_out
    echo
    return 1
  fi
  return $result
}

test_stdout() {
  if [[ -f expected-stdout || -f expected-stdout-partial ]]; then
    docker logs $(cat $cid_file) > ${tmp_dir}/stdout
  fi
  if [ -f expected-stdout ]; then
    if ! diff -q ${tmp_dir}/stdout expected-stdout > /dev/null ; then
      echo "Expected output does match actual output."
      diff -u expected-stdout ${tmp_dir}/stdout
      echo
      return 1
    fi
  fi
  if [ -f expected-stdout-partial ]; then
    partial=$(cat expected-stdout-partial)
    if ! grep -q "${partial}" ${tmp_dir}/stdout ; then
      echo "Unable to find partial in stdout"
      echo "Partial str: ${partial}"
      echo "Stdout:"
      cat ${tmp_dir}/stdout
      echo
      return 1
    fi
  fi
  return 0
}

wait_for_cid() {
  local max_attempts=10
  local sleep_time=1
  local attempt=1
  local result=1
  while [ $attempt -le $max_attempts ]; do
    [ -f $cid_file ] && break
    echo -n "Waiting for container to start...  "
    attempt=$(( $attempt + 1 ))
    sleep $sleep_time
  done
}

for test_app_dir in $(echo ${test_dir}/*/); do
  test_app=$(basename ${test_app_dir})
  echo -n "Running test ${test_app}...  "
  cd ${test_app_dir}

  tmp_dir=$(mktemp -d)
  cid_file="${tmp_dir}/cid"
  out_file="${tmp_dir}/out"

  # Build the image
  echo -n "Building test image...  "
  prepare ${test_app} > ${out_file} 2>&1
  check_result ${test_app} $?

  test_entrypoint > ${out_file} 2>&1
  check_result ${test_app} $?

  # Start the test application
  if [ -f run-in-background ]; then
    run_test_application ${test_app} > ${out_file} &
  else
    run_test_application ${test_app} > ${out_file}
  fi
  wait_for_cid

  test_http > ${out_file} 2>&1
  check_result ${test_app} $?

  test_stdout > ${out_file} 2>&1
  check_result ${test_app} $?

  cleanup ${test_app}

  echo "PASSED"
done

exit 0
